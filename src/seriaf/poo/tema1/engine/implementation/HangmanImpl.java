package seriaf.poo.tema1.engine.implementation;

import java.lang.IllegalArgumentException;
import seriaf.poo.tema1.engine.GameState;
import java.lang.IllegalStateException;
import java.lang.Object;
import java.util.Arrays;
import seriaf.poo.tema1.engine.Hangman;

public class HangmanImpl implements Hangman{
    
    private String mWordToGuess; //cuvantul de ghicit
    private int mNrLifes;// nr vieti
    private int mNrGuessedLetters = 0;// litere ghicite
    private boolean mMatchWon = false;//jocul terminat?
    private String mWord = new String();//cuvantul ghicit pana actual
    private char [] mWordArray;// cuvantul ghicit ca array
    private int mLength;//lungimea cuvantului de ghicit
    
   public HangmanImpl(String word, int length) throws IllegalArgumentException{
       this.mLength = length;
       this.mNrLifes = length;
       this.mWordToGuess = word.toLowerCase(); //converteste cunavtul in litere mici; lafel si "move" pentru a face programul case insensitive
       
       if(!word.matches("[a-zA-Z]*") || mLength < 3 || mNrLifes <= 0){
            throw new IllegalArgumentException("cuvatul trebuie sa contina numai litere");
       }else{
           char[] WordArray = new char[mLength];
           Arrays.fill(WordArray,'_'); // pt afisare "mascata" _____
           this.mWord = String.copyValueOf(WordArray);
           this.mWordArray = WordArray;
       }
               
        
    }
    public GameState makeMove(char move) throws IllegalArgumentException, IllegalStateException{
        
        if(!Character.isLetter(move)){
            throw new IllegalArgumentException("mutarea nu merge");
        }
        if(mMatchWon || mNrLifes == 0){
            throw new IllegalStateException("jocul e gata");
        }
        
        char moveLowerCase = Character.toLowerCase(move); // converteste mutarea in litera mica
        if(mWord.indexOf(moveLowerCase) != -1){
            throw new IllegalArgumentException("litera a fost deja ghicita"); // previne introducerea unei singure mutari corecte in mod repetat pana la castigarea jocului
        }
        if(mWordToGuess.indexOf(moveLowerCase) != -1){
            for (int i = -1; (i = mWordToGuess.indexOf(moveLowerCase, i + 1)) != -1; ) { // fiecare aparitie a "moveLowerCase" in cuvant
                this.mWordArray[i] = moveLowerCase;
                this.mNrGuessedLetters++;
                if(mNrGuessedLetters == mLength){
                    mMatchWon = true;
                }
            }
        }else {
            this.mNrLifes--;
        }
        
        this.mWord = String.copyValueOf(this.mWordArray);//transforma array in string
        GameState status = new GameState(mWord, mNrLifes, mMatchWon);
        return status;
    }
    
    public GameState getGameState(){
        
        this.mWord = String.copyValueOf(this.mWordArray);//transforma array in string
        GameState status = new GameState(mWord, mNrLifes, mMatchWon);
        return status;
    }
    
    
}
