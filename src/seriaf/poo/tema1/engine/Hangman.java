package seriaf.poo.tema1.engine;

/**
 * Interface to be implemented by the Hangman engine.
 */
public interface Hangman {

    /**
     * Makes a new move with the specified character.
     * 
     * @param newChar the new character to try.
     * 
     * @return a GameState reference holding information about the current word state, lives left, and win/lose state.
     * 
     * @throws IllegalStateException if the method is called again after the match is won or lost.
     * @throws IllegalArgumentException if {@see newChar} is not a letter.
     */
    public GameState makeMove(char newChar);
    
    /**
     * @return the current game state, without making a move.
     */
    public GameState getGameState();
}