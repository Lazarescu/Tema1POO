package seriaf.poo.tema1.engine;

/**
 * Class holding state values for a Hangman game.
 */
public class GameState {

    /**
     * The partial word that has been guessed so far.
     */
    private final String mWord;

    /**
     * The number of lives left.
     */
    private final int mLivesLeft;

    /**
     * Flag signaling if the match was won.
     */
    private final boolean mMatchWon;

    /**
     * Class constructor.
     * <p>
     * @param word      The partial word that has been guessed so far.
     * @param livesLeft The number of lives left.
     * @param matchWon  Flag signaling if the match was won.
     */
    public GameState(String word, int livesLeft, boolean matchWon) {
        mWord = word;
        mLivesLeft = livesLeft;
        mMatchWon = matchWon;
    }

    /**
     * @return The partial word that has been guessed so far.
     */
    public String getWord() {
        return mWord;
    }

    /**
     * @return The number of lives left.
     */
    public int getLivesLeft() {
        return mLivesLeft;
    }

    /**
     * @return Flag signaling if the match was won.
     */
    public boolean isMatchWon() {
        return mMatchWon;
    }

    /**
     * Creates a string representation of this game state.
     * @return 
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Cuvantul ghicit: ");
        builder.append(mWord);
        builder.append('\n');
        builder.append("Numarul de vieti ramase: ");
        builder.append(mLivesLeft);
        builder.append('\n');
        if (mLivesLeft == 0) {
            builder.append("Joc pierdut!\n");
        } else if (mMatchWon) {
            builder.append("Joc castigat!\n");
        }
        return builder.toString();
    }
}