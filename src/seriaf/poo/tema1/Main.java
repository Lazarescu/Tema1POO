package seriaf.poo.tema1;

import java.io.Console;
import seriaf.poo.tema1.engine.GameState;
import seriaf.poo.tema1.engine.Hangman;
import seriaf.poo.tema1.engine.implementation.HangmanImpl;

/**
 * Hangman Main class.
 */
public class Main {

    /**
     * Application entry point.
     * 
     * @param args unused.
     */
    public static void main(String[] args) {
        Console console = System.console();
        console.printf("Introduceti cuvantul: ");

        char[] text = console.readPassword();
        String word = new String(text);

        try {
            Hangman hangman = new HangmanImpl(word, word.length());
            GameState state = hangman.getGameState();
            do {
                try {
                    console.printf("Litera: ");
                    state = hangman.makeMove(console.readLine().charAt(0));
                    console.printf(state.toString());
                } catch (IllegalArgumentException ex) {
                    console.printf("Eroare: %s\n", ex.getMessage());
                }
            } while (!state.isMatchWon() && state.getLivesLeft() != 0);
        } catch (IllegalArgumentException ex) {
            console.printf("Eroare: %s\n", ex.getMessage());
        }
    }
}